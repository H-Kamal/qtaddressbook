#include "addressbook.h"

AddressBook::AddressBook(QObject *parent) : QObject(parent)
{

}

AddressBook::Entries AddressBook::entries() const
{
    return m_entries;
}

//Returns pointer to AddressBookEntry
AddressBookEntry* AddressBook::createEntry()
{
    // AddressBookEntry constructer takes a QObject parent.
    // Therefore use "this" to specify AddressBook*
    auto result = new AddressBookEntry(this);
    if (result != nullptr)
    {
        // Adding to linked list of entries
        m_entries.append(result);
        emit entryAdded(result);
    }
    return result;
}

bool AddressBook::deleteEntry(AddressBookEntry *entry)
{
    if (m_entries.contains(entry))
    {
        emit entryDeleted(entry);
        m_entries.removeOne(entry);
        delete entry;
        return true;
    }
    return false;
}




