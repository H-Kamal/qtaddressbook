#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

// Class which creates/deletes an addressbook entry
// This class holds all entries in a linked list and AddressBookEntry holds all info of each entry appropriately

#include "addressbookentry.h"

// Need to create a container for the address book. Could use QMap or QHash as well depending on needs
#include <QList>
#include <QObject>

class AddressBook : public QObject
{
    Q_OBJECT
public:
    // Define Entries as a list of pointers to AddressBookEntry.
    // Each AddressBookEntry has a single name, phone number, birthday, etc.
    typedef QList<AddressBookEntry*> Entries;

    explicit AddressBook(QObject *parent = 0);

    // Getter for the entries member var
    Entries entries() const;

    AddressBookEntry* createEntry();
    // Pass in pointer to entry to delete it
    bool deleteEntry(AddressBookEntry *entry);

signals:
    void entryAdded(AddressBookEntry *entry);
    void entryDeleted(AddressBookEntry *entry);
private:
    // List of pointers to AddressBookEntry
    Entries m_entries;
public slots:
};

#endif // ADDRESSBOOK_H
