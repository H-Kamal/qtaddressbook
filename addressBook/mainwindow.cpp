#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>

// Constructor for the MainWindow class
MainWindow::MainWindow(AddressBookController *controller, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_controller(controller)
{
    Q_ASSERT( controller != nullptr );
    ui->setupUi(this);
    setUpConnections();
}

//Destructor for the MainWindow class
MainWindow::~MainWindow()
{
    delete ui;
}

// Creates a new AddressBookEntry and adds the entry's default name as the list item
// Adds the addressBookEntry to the map so we can correspond the listItem to the entry in future
void MainWindow::createEntry()
{
    auto entry = m_controller->createEntry();
    if (entry)
    {
        // ui corresponds to the things we changed in the design plane
        ui->listWidget->addItem( entry->name() );
        // We populate the m_entryMap map so we can correspond each addressBookEntry to each listWidget item
        auto listItem = ui->listWidget->item(ui->listWidget->count() - 1);
        // Insert the inserted listItem into the map
        m_entryMap.insert(listItem, entry);
    }
}

// Goes to the current list item, finds out which addressBookEntry it corresponds to, and deletes it
void MainWindow::deleteEntry()
{
    auto listItem = ui->listWidget->currentItem();
    if (listItem)
    {
        // Looking up the key listItem to find the addressBookEntry in the m_entryMap map
        auto entry = m_entryMap.value(listItem);
        if (entry)
        {
            // deleteEntry returns true if deletion is successful
            if (m_controller->deleteEntry(entry))
            {
                m_entryMap.remove(listItem);
                delete listItem;
            }
        }
    }
}

void MainWindow::editEntry()
{
    auto listItem = ui->listWidget->currentItem();
    if (listItem)
    {
        // Looking up the key listItem to find the addressBookEntry in the m_entryMap map
        auto entry = m_entryMap.value(listItem);
        if (entry)
        {
            // Changes the page which is a stackedWidget to the second page called detailPage
            ui->stackedWidget->setCurrentWidget(ui->detailPage);
            // Removes drop down add/remove/edit menu when in editing page
            ui->menuEntries->setEnabled(false);
            // Initializes the contents of all the widgets (name, address, etc) by calling the getter method
            // If it's a brand new entry, nothind has changes since constructors which were just blank
            resetEntry();
        }
    }

}

// Calls setter of each AddressBookEntry and populates with whatever is in the ui fields
void MainWindow::saveEntry()
{
    auto listItem = ui->listWidget->currentItem();
    if (listItem)
    {
        // Looking up the key listItem to find the addressBookEntry in the m_entryMap map
        auto entry = m_entryMap.value(listItem);
        if (entry)
        {
            entry->setName(ui->nameEdit->text());
            entry->setBirthday(ui->birthdayEdit->date());
            entry->setAddress(ui->addressEdit->toPlainText());
            entry->setPhoneNumbers(ui->phoneNumberEdit->toPlainText().split("\n"));
            listItem->setText(entry->name());

            ui->stackedWidget->setCurrentWidget(ui->ListPage);
            ui->menuEntries->setEnabled(true);
        }
    }
}

void MainWindow::discardEntry()
{
    // Changes the page back to the list of contacts
    ui->stackedWidget->setCurrentWidget(ui->ListPage);
    // Re-enables usage of add/remove/edit menu bar
    ui->menuEntries->setEnabled(true);
}

void MainWindow::resetEntry()
{
    auto listItem = ui->listWidget->currentItem();
    if (listItem)
    {
        // Looking up the key listItem to find the addressBookEntry in the m_entryMap map
        auto entry = m_entryMap.value(listItem);
        if (entry)
        {
            // Initializes the contents of all the widgets (name, address, etc) by calling the getter method
            // If it's a brand new entry, nothind has changes since constructors which were just blank
            ui->birthdayEdit->setDate(entry->birthday());
            ui->addressEdit->setPlainText(entry->address());
            ui->phoneNumberEdit->setPlainText(entry->phoneNumbers().join("\n"));
        }
    }
}

void MainWindow::setUpConnections()
{
    // connect is a method of QObject
    // Whenever actionAdd is clicked, a signal is triggered, and the createEntry slot of this class is executed
    //connect(ui->actionAdd, SIGNAL(triggered(bool)), this, SLOT(createEntry()));
    connect(ui->actionAdd, &QAction::triggered, this, &MainWindow::createEntry);
    connect(ui->actionRemove, &QAction::triggered, this, &MainWindow::deleteEntry);
    connect(ui->actionEdit, &QAction::triggered, this, &MainWindow::editEntry);
    connect(ui->buttonBox->button(QDialogButtonBox::Save), &QPushButton::clicked, this, &MainWindow::saveEntry);
    connect(ui->buttonBox->button(QDialogButtonBox::Reset), &QPushButton::clicked, this, &MainWindow::resetEntry);
    connect(ui->buttonBox->button(QDialogButtonBox::Discard), &QPushButton::clicked, this, &MainWindow::discardEntry);
}
