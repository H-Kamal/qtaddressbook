#ifndef ADDRESSBOOKCONTROLLER_H
#define ADDRESSBOOKCONTROLLER_H

#include <QObject>
#include "addressbook.h"

class AddressBookController : public QObject
{
    Q_OBJECT
public:
    // Changed the constructor arguement since we want to use the "dependency injection" design pattern
    explicit AddressBookController(AddressBook *addressBook, QObject *parent = 0);

    AddressBookEntry* createEntry();
    bool deleteEntry(AddressBookEntry* entry);

signals:

private:
    AddressBook *m_addressBook;
public slots:
};

#endif // ADDRESSBOOKCONTROLLER_H
