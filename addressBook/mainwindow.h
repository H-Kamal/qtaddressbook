#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include "addressbookcontroller.h"

namespace Ui {
class MainWindow;
}

class QListWidgetItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(AddressBookController* controller, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void createEntry();
    void deleteEntry();
    void editEntry();
    void saveEntry();
    void discardEntry();
    void resetEntry();

private:
    Ui::MainWindow *ui;
    AddressBookController* m_controller;
    // Maps a QListWidgetItem to an AddressBookEntry
    QHash<QListWidgetItem*, AddressBookEntry*> m_entryMap;

    //Private helper method
    void setUpConnections();
};

#endif // MAINWINDOW_H
