#-------------------------------------------------
#
# Project created by QtCreator 2019-12-14T09:59:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = addressBook
# Creates an application. Ex: and .exe for Windows
TEMPLATE = app

# This compile flag automatically
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    addressbookentry.cpp \
    addressbook.cpp \
    addressbookcontroller.cpp

HEADERS  += mainwindow.h \
    addressbookentry.h \
    addressbook.h \
    addressbookcontroller.h

FORMS    += mainwindow.ui
