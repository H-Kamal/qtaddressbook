#include "addressbookcontroller.h"

// Add in the addressBook paremter for dependance injection desgin pattern.
// Use initalization list to initialize m_addressBook with the dependancy
AddressBookController::AddressBookController(AddressBook *addressBook, QObject *parent) :
    QObject(parent), m_addressBook(addressBook)
{
    // Assertion requires that addressBook is initialized as non null
    Q_ASSERT( addressBook != nullptr);
}

AddressBookEntry *AddressBookController::createEntry()
{
    // This calls the AddressBook class' createEntry. Not the controlerr's
    auto result = m_addressBook->createEntry();
    if (result != nullptr)
    {
        result->setName( tr("New Entry...") );
    }
    return result;
}

bool AddressBookController::deleteEntry(AddressBookEntry *entry)
{
    return m_addressBook->deleteEntry(entry);
}
